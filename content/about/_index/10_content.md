+++
fragment = "content"
weight = 10
background = "white"

title = "Was es über uns zu wissen gibt"
+++

velocitux ist ein junges Unternehmen. Gegründet im April 2022,
wagen wir den Schritt, während zwei Jahrzehnten gesammelte Erfahrungen
unter einem neuen Namen anzubieten.

Dabei bündeln wir berufliche Erfahrung in der Software-Entwicklung,
in der Systemadministration, Beratung und im Endkunden-Support mit
langjähriger Praxis aus der Open-Source- und Freie-Software-Gemeinschaft.

"Wir", das ist neben mir, [Dominik George]({{< ref "about/people" >}}),
ein Netzwerk aus Expertinnen und Experten der Freie-Software- und
Open-Source-Gemeinschaft. Obwohl die velocitux UG selber ein
Ein-Personen-Unternehmen ist, stehen über uns auch weitere Partnerinnen
und Partner zur Verfügung, die in Projekten zusammenarbeiten.
