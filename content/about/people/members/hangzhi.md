+++
title = "Hangzhi Yu"
weight = 20
date = "2022-12-01"

position = "Full-Stack-Webentwickler"
#company = ""

lives_in = "Berlin"

[[icons]]
  icon = "fas fa-envelope"
  url = "mailto:hangzhi@velocitux.com"

[asset]
image = "hangzhi.jpg"
+++

Hangzhi Yu sammelte seine ersten Erfahrungen mit der Entwicklung von
Freier Software schon während seiner Schulzeit im
[AlekSIS](https://aleksis.org/de/)-Projekt, in dem er bis heute aktiv ist.
Aktuell studiert er in Berlin Politikwissenschaft. Er unterstútzt
als Werkstudent bei velocitux Entwicklungsprojekte im Frontend und Backend.