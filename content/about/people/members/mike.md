+++
title = "Mike Gabriel"
weight = 50
date = "2022-05-07"

position = "Debian-Experte (Extern)"
company = "DAS-NETZWERKTEAM"

lives_in = "Schleswig-Holstein"

[[icons]]
  icon = "fas fa-envelope"
  url = "mailto:mike.gabriel@das-netzwerkteam.de"

[asset]
image = "mike.jpg"
+++

Mike Gabriel, Linux-Expertise und FLOSS-Freelancer seit 1999.
Seit 2013 Debian-Entwickler mit verschiedenen Themenschwerpunkten
(Debian Edu, [MATE-Desktop-Environment](https://mate-desktop.org/),
[Horde-Groupware](https://www.horde.org/apps/groupware) sowie verschiedene
Komponenten von anwenderfreundlichen Desktop-Umgebungen und
Terminal-Server-Lösungen. Seit 2021 ist er Mitentwickler von
[Ubuntu Touch](https://ubuntu-touch.io/) und leitet seit 2020 als CEO die
[Fre(i)e Software GmbH](https://freiesoftware.gmbh/).
