+++
fragment = "content"
weight = 5
date = "2022-04-03"

background = "white"
title = "Menschen rund um velocitux"
+++

Das Wichtigste bei unserer Leistung sind die Kompetenzen und
Erfahrungen der Menschen, die Sie beraten und Ihre Projekte
umsetzen.

Hier stellen wir diejenigen vor, die gemeinsam unsere Ziele
verfolgen und Projekte in Beratung, Support und Entwicklung
mit ihrem Wissen und Können umsetzen.
