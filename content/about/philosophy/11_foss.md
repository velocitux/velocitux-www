+++
fragment = "item"
weight = "11"
align = "right"

background = "light"

title = "Freie Software und Open Source"

[asset]
icon = "fab fa-osi"
+++

Freie Software ist der Grundstein und die Voraussetzung für
vertrauenswürdige und nachvollziehbare IT, denn die Offenlegung
des Programmcodes schafft Transparenz. Neben dem offenen Programmcode
bietet freie Software aber, insbesondere für Unternehmen, viele
weitere Vorteile:

* Volle Kostenkontrolle durch entfallende Lizenzkosten
* Anbieterunabhängigkeit durch offene Technologie und Schnittstellen
* Anpassbarkeit durch die Möglichkeit, eigene Anpassungen vornehmen
  zu lassen

Bei velocitux garantieren wir, ummer zu 100% freie und offene Software
einzusetzen, selber ausschließlich freie Software zu entwickeln und
die Menschen, auf deren Open-Source-Arbeit unsere Lösungen aufbauen
nach bester Móglichkeit zu unterstützen.
