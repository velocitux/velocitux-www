+++
fragment = "item"
weight = "14"
align = "left"

background = "secondary"

title = "Bildung und Lernen"

[asset]
icon = "fas fa-graduation-cap"
+++

Erfahrung und Wissen kommen nicht über Nacht und wir sind überzeugt,
dass nichts nachhaltig bildet wie Praxis und persönlicher Austausch
mit Gleichgesinnten. Deshalb unterstützen wir jede Form von digitaler
Bildung.

* velocitux berät mit Vorliebe Bildungsanbieter wie Schulen,
  Schulungszentren, Makerspaces und Jugendeinrichtungen
* Unsere Mitarbeitenden, Partnerinnen und Partner geben ihr Wissen
  in Schulungen weiter
* Praktikantinnen und Praktikanten jeden Alters sind bei uns willkommen
  und werden ernstgenommen
* Wir nehmen aktiv an Konferenzen teil und fördern den Austausch mit
  der Gemeinschaft
