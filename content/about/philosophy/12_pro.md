+++
fragment = "item"
weight = "12"
align = "left"

background = "white"

title = "Kompetenz"

[asset]
	icon = "fas fa-lightbulb"
+++

Fachkompetenz zeichnet jeden Berater, Systemadministrator und
Entwickler aus und kompetente Mitarbeitende und Dienstleister sind
ein wichtiger Stützpfeiler für eine zuverlässige und sichere IT-Umgebung.
Deshalb ist es uns wichtig, zu wissen, wovon wir reden!

* Unsere Mitarbeitenden, Partnerinnen und Partner sind Menschen aus
  der Praxis und begeistern sich, auch außerhalb ihrer Arbeitszeit,
  für die IT- und Technik-Themen, zu denen wir Leistungen anbieten
* Wir haben langjährige Erfahrung, die wir in der Praxis in verschiedensten
  Umgebungen erlernt haben
* Austausch von Wissen und neuen Entwicklungen mit anderen Menschen steht
  bei uns auf der Tagesordnung

Bei velocitux gibt es garantiert keine Beratung durch Theoretiker, sondern
Wissen und Erfahrung von kompetenden Menschen!
