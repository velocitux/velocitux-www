+++
fragment = "item"
weight = 20

title = "Ticketsystem und ERP"
background = "dark"
align = "right"

[[buttons]]
  text = "Ticket erstellen"
  url = "https://erp.velocitux.com/public/ticket/index.php"
  color = "primary"
+++

Wenn Sie bereits Support-Kunde sind oder Ihre Neuanfrage über ein
Web-Formular stellen möchten, können Sie stattdessen auch unser Ticketsystem
verwenden.

Als bestehender Kunde oder Interessent haben Sie hier auch die Möglichkeit,
Ihre offenen Anfragen einzusehen.
