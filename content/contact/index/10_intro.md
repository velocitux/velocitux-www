+++
fragment = "item"
weight = 10

title = "Kontakt zu velocitux"
background = "white"
align = "left"

[[buttons]]
  text = "info@velocitux.com"
  url = "mailto:info@velocitux.com"
  color = "dark"
+++

Haben Sie Fragen zu unseren Leistungen oder schon eine konkrete Anfrage?
Wir freuen uns, wenn Sie mit uns Kontakt aufnehmen. Das funktioniert am Besten
per **E-Mail**. Sollten Sie uns lieber telefonisch sprechen wollen, rufen wir
gerne zurück.
