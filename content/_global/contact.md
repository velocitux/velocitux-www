+++
fragment = "buttons"
weight = 1100
background = "primary"

title = "Fragen?"
subtitle = "Gerne stehen wir jederzeit für Ihre Anfragen bereit."

[[buttons]]
  text = "Jetzt Kontakt aufnehmen"
  url = "/contact"
  color = "dark"
+++
