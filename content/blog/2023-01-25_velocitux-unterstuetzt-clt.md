+++
fragment = "content"
date = "2023-01-25"
weight = 10
title = "velocitux unterstützt die Chemnitzer Linux-Tage"
background = "white"

summary = "Am 11. und 12. Márz 2023 laden die Chemnitzer Linux-Tage wieder in die TU Chemnitz ein. velocitux unterstützt die Konferenz als Community-Sponsor."

display_date = true

[asset]
image = "clt-2023-sponsor.webp"
+++

Bereits zum 25. Mal findet Deutschlands, nach Zählung der Veranstalter, größte Linux- und
Open-Source-Konferenz in den Räumen der [TU Chemnitz](https://www.tu-chemnitz.de/) statt.
Die [Chemnitzer Linux-Tage](https://chemnitzer.linux-tage.de/2023/de/), ausgerichtet vom
[Individual Network Chemnitz](https://www.in-chemnitz.de/), bieten jedes Jahr knapp 100
Vorträge, Workshops, eine große Projektausstellung sowie viele Besonderheiten wie die
Praxis Dr. Tux und ein Kinder- und Jugendprogramm, an dem auch unser Bildungspartner
[Teckids e.V.](https://teckids.org/) beteiligt ist.

Unser Unternehmen hat sich unter anderem [zum Ziel gesetzt](https://www.in-chemnitz.de/),
mit dem Ertrag unserer Arbeit direkt zu echter freier und offener Software beizutragen.
Deshalb sind wir stolz, bereits im ersten Unternehmensjahr als Community-Sponsor eine
Veranstaltung wie die Chemnitzer Linux-Tage auch finanziell unterstützen zu können.

## Studierende mit Interesse an echter digitaler Souveränität gesucht

Für das freie Schul-Informations-System [AlekSIS®](https://aleksis.org/de/),
das auch mit einem Projektstand auf den Chemnitzer Linux-Tagen vertreten
sein wird, suchen wir Werkstudent\*innen für die Entwicklung und für die
Ausarbeitung automatisierter Tests. Die Stellenbörse der Chemnitzer
Linux-Tage erscheint uns als ideale Möglichkeit, interessierte junge
Entwickler\*innen zu erreichen.

Schon jetzt gibt es weitere Informationen sowie eine Bewerbungsmöglichkeit
in unseren Stellenausschreibungen:

 * [Werkstudent*in oder Minijob AlekSIS-Entwicklung](https://erp.velocitux.com/public/recruitment/view.php?ref=JOB2212-0001)
 * [Werkstudent*in oder Minijob AlekSIS: QA und Testing](https://erp.velocitux.com/public/recruitment/view.php?ref=JOB2301-0002)

## Community und Wirtschaft Hand in Hand

Dass sich Freie Software und Open Source nicht von selber tragen, ist eine oft unterschätzte
Tatsache. Zwar gibt es eine schier endlose Sammlung an ausschließlich freiwillig betreuter
Software und nicht immer esakaliert das Problem in einer Form, wie kürzlich beim
[Maintainer-Burnout des Entwicklers Marak](https://www.golem.de/news/open-source-entwickler-sabotiert-eigene-vielfach-genutzte-npm-pakete-2201-162299.html),
der aus Frustration über die mangelnde Bereitschaft großer Wirtschaftsunternehmen,
Open-Source-Software an ihrem Gewinn zu beteiligen, zwei seiner meistgenutzten Pakete
unbrauchbar machte, jedoch sollte jeder wirtschaftlich Abhängige in der Lage sein, sich
in die Situation der ehrenamtlichen Entwickler\*innen hineinzuversetzen.

Die Freiheit von Open-Source-Software ist ein hohes und wichtiges Gut, das eine
konsequente Ideologie benötigt. Lizenzmodelle, die die Nutzung für gewinnorientierte
Unternehmen generell einschränken, sind daher keine hilfreiche Lösung und widersprechen
den grundlegenden [Software-Freiheiten](https://fsfe.org/freesoftware/freesoftware.de.html).
Umso wichtiger ist es, dass Unternehmen, die freie Software nutzen, fair mit deren
Entwickler\*innen und Maintainer\*innen umgehen und freiwillig Beiträge leisten, sowohl
durch personelle Beteiligung als auch durch finanzielle Zuwendung.

Als IT-Unternehmen, das voll und ganz hinter der Ideologie freier Software steht,
probiert velocitux daher kontinuierlich verschiedene Wege aus, die Gemeinschaft
an wirtschaftlichen Erträgen zu beteiligen und motiviert seine Kund\*innen, ebenfalls
im Rahmen ihrer Möglichkeiten Beiträge zum Ökosystem zu leisten.
