+++
fragment = "content"
date = "2023-01-30"
weight = 10
title = "AlekSIS as a Dayjob: Als Werkstudent im FOSS-Bildungs-Projekt"
background = "white"

summary = "Bei velocitux arbeiten auch junge Menschen, ausschließlich mit und an Freier Software – unter anderem am freien Schulinformationssystem AlekSIS. Was bedeutet die Möglichkeit, für das Entwicklung offener Bildungssoftware bezahlt zu werden für Werkstudent*innen?"

display_date = true

#[asset]
#image = "hangzhi.jpg"
#align = "right"
+++

Entwicklung von freier Open-Source-Software – das klingt intuitiv nach einer
Menge freiwilliger Arbeit. Genauso sah und sieht es bei dem
Schulinformationssystem [AlekSIS®](https://aleksis.org/de/) aus. Geboren aus der
[Fusion zweier sehr ähnlicher Vorgängerprojekte](https://aleksis.org/de/news/2021/10/aleksis-und-seine-vorg%C3%A4nger-feiern-zehnten-geburtstag/), entwickelte sich AlekSIS durch
die ehrenamtliche Arbeit einer kleinen, engagierten Entwicklergruppe in den
letzten Jahren zu einer umfangreichen und erweiterbaren Softwarelösung für
vielfältige Anwendungsbereiche in nicht nur schulischen Kontexten. Mittlerweile
wird es an mehreren Schulen im Produktivbetrieb eingesetzt – und wir von
velocitux bieten professionellen Betrieb, Support und Anpassungen am Code an.

Mein Name ist Hangzhi und ich bin seit ein paar Monaten Werkstudent bei
velocitux. Zu meiner Arbeit gehört vor allem, auf Wunsch von Kunden – das
können zum Beispiel Schulen oder kommunale Schulträger sein – Anpassungen
in Form von gewünschten Features in AlekSIS zu konzipieren und zu
implementieren. Im Grunde also von der reinen Arbeit her nichts
Grundverschiedenes als das, was ich in meiner ehrenamtlichen Arbeit für das
Projekt schon seit vielen Jahren seit der Mittelstufe gemacht habe.

Vieles hat sich mit der Möglichkeit des bezahlten Arbeitens an AlekSIS
verändert. Zunächst ein wenig überraschend, aber mir am wichtigsten ist das Mehr
an Freiheit, das mit bezahlter Arbeit einherging. Oder genauer gesagt: Die
Freiheiten, die ich am ehrenamtlichen Arbeiten sehr schätze – “Arbeiten nach
Interesse”, relativ freie Zeiteinteilung, wenig Abhängigkeit von Externen –
werden durch andere ergänzt. Mittlerweile brauche ich mir zum Beispiel kaum
Stress zu machen, wenn ich versuche, möglichst regelmäßige AlekSIS-Arbeit,
anderes Engagement, mein Studium und alles andere Unbezahlte im Leben unter
einen Hut zu bringen und dabei immer den nervigen Gedanken im Hinterkopf habe,
dass man zwischen all diesen Dingen noch irgendwie eine Erwerbstätigkeit
unterbringen müsste. Das Abwägen zwischen eigenen Interessen und Arbeit aus rein
finanziellen Gründen raubt in vielen Fällen die Freiheit, die eigene Zeit
selbstbestimmt und sinnstiftend einzuteilen.

Klar: Bezahlt werden bedeutet auch, ein gewisses Maß an Verbindlichkeit
hinzunehmen. Die grobe Richtung meiner Arbeit ist jetzt Ergebnis einer Absprache mit
externen Akteuren und nicht mehr einer rein internen Priorisierung.
Deadlines und strukturierte Aufgabenplanung waren nie meine besten
Freunde, und anfangs hatte ich bisweilen meine Schwierigkeiten damit, im Voraus
meine zu erledigende Arbeit einzuteilen. Gleichzeitig bin ich im Nachhinein froh
darüber, dass diese Zunahme an Verbindlichkeit und Struktur ein bisschen auf das
gesamte AlekSIS-Projekt abgefärbt hat – und dafür sorgt, dass wir in unserer
freiwilligen Arbeit stressfreier und trotzdem schneller vorankommen und besser
in der Lage sind, die zukünftige Entwicklung des Projektes zu steuern. Ich
empfinde es auch immer wieder als herausfordernd und zugleich bereichernd, mich
im Rahmen von externen Aufträgen mit Teilbereichen der Webentwicklung zu
beschäftigen und schließlich Dinge zu lernen, mit denen ich mich sonst nie
auseinandergesetzt hätte. Man könnte wohl sagen, dass das bezahlte Arbeiten am
Projekt meinen technischen Horizont zwangsläufig erweitert hat.

Und auch im Allgemeinen passen Freie Software und Erwerbstätigkeit für mich
gut zusammen:: FOSS und Profitorientierung sind kein natürliches Begriffspaar.
Sie schließen sich aber auch nicht aus, denn Freiheit impliziert in der üblichen
FOSS-Definition keine Kostenfreiheit. Die Entwicklung und Anwendung von FOSS findet
nicht in einer unkommerziellen Parallelwelt statt, sondern ganz wesentlich im aktuellen,
mehrheitlich gewinnorientierten Wirtschaftssystem – und das ist auch vollkommen
okay so, solange die Idee der Profitorientierung an sich nicht infrage gestellt
wird. Ich bin davon überzeugt, dass das Maß an Verbreitung, das Freie Software in diversen
Bereichen mittlerweile erreicht hat, ohne die Arbeit von gewinnorientierten
Unternehmen und Einzelpersonen nie möglich gewesen wäre. Denkt man zum Beispiel
an per definitionem viel externen Support benötigende Bereiche der IT, zum
Beispiel Webhosting-Dienstleistungen, dann ist es schwer vorstellbar, wie ein
großflächiger Einsatz ohne ein solides Gerüst an externen
Unterstützungsdienstleistern möglich sein soll. Auch ergibt es in vielen Fällen
für (mitunter gewinnorientierte) Institutionen selber Sinn, teils auf FOSS zu
setzen, zum Beispiel, um einen Lock-in auf proprietäre Software zu verhindern –
was ein Teilaspekt der Freiheit im Sinne der Freie-Software-Bewegung ist.

velocitux bietet ausschließlich Dienstleistungen im Kontext von freier
Open-Source-Software an. Das machen wir, weil wir, die Menschen rund um
velocitux, im Laufe der Jahre vielfältige Erfahrungen in der Arbeit mit FOSS
gesammelt haben und gerade deswegen davon überzeugt sind, dass die dazugehörigen
[Ideale](/about/philosophy/) zu einer besseren Welt beitragen können – denn sie schafft Transparenz
und Unabhängigkeit für Nutzer\*innen, sichert Teilhabe für Interessierte, betont
einen Anspruch auf Gleichheit im finanziellen Sinne und schafft Vertrauen in
Anwendungen, auf die wir alle zunehmend angewiesen sind.

Und am Ende muss "Profit" nicht "Bereicherung" bedeuten: Stattdessen ist
es uns ein Herzensanliegen, mit erwirtschafteten Mitteln wieder echte
Open-Source-Projekte wie AlekSIS zu stärken – auch in Aspekten, die wir
als ehrenamtliches Entwicklerteam nicht ausreichend leisten könnten.

Die Möglichkeit, bestimmte Features gegen Entgelt implementieren zu lassen und
professionelle Unterstützung im gesamten Prozess von der Idee, ein
Schulinformationssystem einzusetzen, bis zur tatsächlichen Inbetriebnahme in
Anspruch nehmen zu können, rundet für Schulen und Schulträger das Gesamtpaket ab und
ermöglicht erst eine großflächige Verbreitung der Software.

So bedeutet "Profit" letzten Endes einen Gewinn für alle Beteiligten: Für
die Entwickler\*innen, da sie für ihre Arbeit entlohnt und entlastet werden
sowie schon früh praktische Berufserfahrungen sammeln können,
für die Nutzer\*innen und Kunden, weil sie zuverlässigen Support erhalten
können und für das gesamte Ökosystem, weil erwirtschaftete Mittel wieder
gezielt zurückinvestiert werden können.

---

Hangzhi ist Student in Politikwissenschaften an der FU Berlin
und war als Schüler bereits einer der Initiatoren des
AlekSIS-Vorgängerprojekts SchoolApps an seiner Schule
[Katharineum zu Lübeck](https://katharineum.de/). Als einer der ersten
Werkstudenten bei velocitux begleitet er momentan insbesondere die Migration
der Software zu einem Vue.js-basierten Frontend, die Teil der
[Roadmap für die Releases 2023.1 und
2023.6](https://aleksis.org/de/news/2022/08/aussicht-releases-2023.1-und-2023.6/)
ist.

velocitux ist [Partner des
AlekSIS-Projekts](https://aleksis.org/de/partners/) für professionelle Entwicklung
und Support. AlekSIS® ist eine eingetragene Wortmarke des Open-Source-Projektes AlekSIS,
vertreten durch den [Teckids e.V.](https://teckids.org/).
