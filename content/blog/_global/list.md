+++
fragment = "list"
weight = 100

sort_order = "desc"

display_date = true
images = true
tiled = true
count = "3"

background = "dark"
title = "Neueste Blog-Posts"
+++
