+++
fragment = "item"
weight = "11"
align = "right"

background = "light"

title = "Beratung / Consulting"

[asset]
icon = "fas fa-user-tie"
+++

Ob die Konzeptionierung einer neuen IT-Umgebung, die Ablösung
bestehender Systeme oder die Auswahl einzelner Komponenten
ansteht – Beratung von Menschen mit praktischer Erfahrung ist
einer der wichtigsten Bausteine. Deshalb steht velocitux Ihnen
zur Seite, um Ihre großen und kleinen Fragen zu beantworten und
im Zusammenhang mit Ihren Anforderungen zu besprechen.

* Welche freie Software passt am Besten zu meiner Anforderung?
* Wie lässt sich eine Open-Source-Komponente am Besten in meine
  bestehende Umgebung integrieren?
* Was ist beim Aufbau meiner IT-Infrastruktur zu beachten, so dass
  alles zueinander und zu meinen Anforderungen passt?

velocitux bietet Ad-Hoc-Beratung sowie Begleitung bei längerfristigen
Projekten.
