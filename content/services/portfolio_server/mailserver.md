+++
weight = 11
title = "E-Mail-Server"

#[asset]
#image = "debian.png"
#url = "https://debian.org"
+++

E-Mail ist und bleibt ein wichtiges Medium, und ein geschäftskritisches
obendrein. Wegen der Herausforderungen durch Spam-Abwehr ist der Betrieb
eigener E-Mail-Infrastruktur jedoch heute sehr komplex.

Wir helfen mit dem betrieb eigener Mail-Server für Empfang und Versand
auf Basis von Postfix, Dovecot, und rspamd, inkl. erweiterter Themen wie
SPF, DKIM, DMARC und E-Mail-Reputation.
