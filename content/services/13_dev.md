+++
fragment = "item"
weight = "13"
align = "right"

background = "light"

title = "Anpassung und Entwicklung"

[asset]
icon = "fas fa-laptop-code"
+++

Anpassbarkeit ist die wohl größte Stärke von freier Open-Source-Software.
Dank der offenen Lizenzen haben Sie die Freiheit, Komponenten genau
auf Anforderungen und Wünsche abzustimmen. Deshalb bieten wir ergänzend
zu Beratung und Support von Open-Source-Komponenten auch:

* Analyse von Anpassungs- und Erweiterungsmöglichkeiten
  von bestehenden Open-Source-Komponenten
* Auswahl gut erweiter- und anpassbarer Software-Komponenten
* Entwicklung von Add-Ons für Open-Source-Software
* Entwicklung neuer Open-Source-Komponenten nach Bedarf
