+++
weight = 10
title = "AlekSIS"

[asset]
image = "aleksis.png"
url = "https://aleksis.org"
+++

AlekSIS® ist ein freies und sehr umfangreiches Schul-Informations-System,
das viele Bereiche der Verwaltung und Organisation sowie des Informationsflusses
in Schulen, Jugendeinrichtungen und Makerspaces digitalisiert.

Wir bieten den Betrieb von AlekSIS, die Integration mit anderen Systemen sowie
umfangreiche Anpassungen.
