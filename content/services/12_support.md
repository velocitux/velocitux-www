+++
fragment = "item"
weight = "12"
align = "left"

background = "white"

title = "Hilfe und Support"

[asset]
	icon = "fas fa-book-medical"
+++

Manchmal passiert Unvorhergesehenes und es wird Rat benötigt,
um ein Problem zu beheben. Oder man möchte eine Expertin oder
einen Experten an der Hand haben, der zuverlässig im Falle
eines Falles zur Seite steht.

* Wir helfen bei akuten Fragen und Problemen zu bestehenden
  Open-Source-Komponenten
* Nach erfolgter Beratung unterstützen wir beim Betrieb und bei
  Auffälligkeiten der ausgewählten Komponenten
