+++
fragment = "content"
weight = "10"
background = "white"

title = "Angebot und Leistungen"
+++

Zum IT-Betrieb mit freier Software und Open Source gehört ein
breites Spektrum an Tätigkeiten und Dienstleistungen. Dabei können
wir Ihnen in mehreren Bereichen helfen. Dank der Verwendung offener
Standards und Open-Source-Software können Sie jedoch selbstverständlich
auch mehrere Partner in Kooperation beauftragen.
