+++
fragment = "list"
weight = 50

sort_order = "desc"

display_date = true
images = true
tiled = true
count = "2"

section = "blog"

background = "white"
title = "Neueste Blog-Posts"
+++
