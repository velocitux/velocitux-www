+++
fragment = "item"
weight = "21"
align = "left"

background = "light"

title = "Service aus einer Hand. Oder in Kooperation."

[asset]
icon = "fas fa-hand-holding-heart"

[[buttons]]
text = "Alle Angebote und Leistungen"
url = "/services"
+++

Zum IT-Betrieb mit freier Software und Open Source gehört ein
breites Spektrum an Tätigkeiten und Dienstleistungen. Dabei können
wir Ihnen in mehreren Bereichen helfen. Dank der Verwendung offener
Standards und Open-Source-Software können Sie jedoch selbstverständlich
auch mehrere Partner in Kooperation beauftragen.

* [Beratung]({{< ref "/services#11_consulting" >}}) bei der Konzeptionierung
  von IT-Umgebungen auf Basis Freier Software oder bei der Auswahl einzelner
  Komponenten
* [Support]({{< ref "/services#12_support" >}}) für große und kleine
  Open-Source-Anwendungen und deren Umgebung
* [Entwicklung]({{< ref "/services#13_dev" >}}) von Anpassungen und Add-Ons
  zu bestehender freier Software oder neuer Anwendungen
* [Schulung]({{< ref "/services#14_training" >}}) zu Themen aus Administration,
  Betrieb und Entwicklung
