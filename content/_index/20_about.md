+++
fragment = "item"
weight = "20"
align = "right"

background = "secondary"

title = "Professionell. Grün. Offen."

[asset]
icon = "fas fa-thumbs-up"

[[buttons]]
text = "Mehr über uns und unsere Ziele"
url = "/about/philosophy"
+++

Willkommen bei velocitux, dem professionellen und grünen
Support-Center für Open Source und Freie Software!

Bei velocitux kombinieren wir alles, was uns wichtig ist,
und das für immer:

* [Freie Open-Source-Software]({{< ref "/about/philosophy#11_foss" >}}), denn Technologie
  und Wissen sollten ohne Einschränkungen frei verfügbar, transparent
  und begreifbar sein
* [Kompetenz]({{< ref "/about/philosophy#12_pro" >}}), denn IT-Systeme und Software
  müssen zuverlässig sein und Vertrauen schaffen
* [Ökologie]({{< ref "/about/philosophy#13_eco" >}}), denn wir alle gemeinsam
  können unser Wissen einsetzen, um ressourcenschonend und sparsam
  mit unserer Umwelt und Zukunft umzugehen
* [Bildung]({{< ref "/about/philosophy#14_edu" >}}), denn Lernen und Verstehen sind
  in unserer technisierten Welt unabdingbar, um langfristig und effektiv
  mitgestalten zu können
